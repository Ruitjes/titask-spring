package titask.api.service;

import titask.api.entity.Col;
import titask.api.entity.dto.ColDTO;
import titask.api.repository.ColRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ColService {
    @Autowired
    private ColRepository repository;

    public String deleteCol(int id) {
        repository.deleteById(id);

        return "Col removed !! " + id;
    }
    
    public String updateCol(int id, ColDTO col) {

        Col persistentCol = new Col();
        persistentCol.setId(col.getId());

        repository.save(persistentCol);

        return "Col updated" + id;

    }
}
