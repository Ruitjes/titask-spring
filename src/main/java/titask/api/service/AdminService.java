package titask.api.service;

import java.nio.file.attribute.UserPrincipal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import titask.api.entity.Board;
import titask.api.entity.Col;
import titask.api.entity.Role;
import titask.api.entity.Task;
import titask.api.entity.User;
import titask.api.entity.adminResponse.BoardColsResponse;
import titask.api.entity.adminResponse.BoardTasksResponse;
import titask.api.entity.adminResponse.UserBoardResponse;
import titask.api.entity.adminResponse.UserResponse;
import titask.api.exception.CustomException;
import titask.api.repository.BoardRepository;
import titask.api.repository.ColRepository;
import titask.api.repository.TaskRepository;
import titask.api.repository.UserRepository;
import titask.api.security.JwtTokenProvider;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import io.swagger.models.Response;

@Service
public class AdminService {

  private UserRepository userRepository;
  private BoardRepository boardRepository;
  private ColRepository colRepository;
  private TaskRepository taskRepository;


  private PasswordEncoder passwordEncoder;
  private JwtTokenProvider jwtTokenProvider;
  private AuthenticationManager authenticationManager;
  
  public AdminService(UserRepository userRepository,BoardRepository boardRepository,ColRepository colRepository, TaskRepository taskRepository, PasswordEncoder passwordEncoder, JwtTokenProvider jwtTokenProvider, AuthenticationManager authenticationManager) {
      this.userRepository = userRepository;
      this.boardRepository = boardRepository;
      this.colRepository = colRepository;
      this.taskRepository = taskRepository;

      this.passwordEncoder = passwordEncoder;
      this.jwtTokenProvider = jwtTokenProvider;
      this.authenticationManager = authenticationManager;
  }
  ///////////////

  public List<UserResponse> getAllUsers() {
    List<User> u = userRepository.findAll();
    List<UserResponse> urList = new ArrayList<>();

    for (User user : u) {
      UserResponse ur = new UserResponse();
      ur.setId(user.getId().toString());
      ur.setEmail(user.getEmail());
      ur.setUsername(user.getUsername());
      ur.setRole(user.getRoles().toString());
      ur.setBoards(user.getBoards().size());
      urList.add(ur);
    }

    return urList;
  }

  public String removeUser(Integer id) {
    userRepository.deleteById(id);

    return "Deleted user with id " + id.toString();
  }

  ///////////////

  public List<UserBoardResponse> getAllUsersBoards() {
    List<User> u = userRepository.findAll();
    List<UserBoardResponse> ubrList = new ArrayList<>();

    for (User user : u) {
      List<Board> b = user.getBoards();
        for(Board board : b){
            UserBoardResponse ubr = new UserBoardResponse();
            ubr.setId(board.getId().toString());
            ubr.setTitle(board.getTitle());
            ubr.setUsername(user.getUsername());
            ubr.setDescription(board.getDescription());
            ubr.setCols(board.getCols().size());
            ubr.setTasks(board.getTasks().size());
    
            ubrList.add(ubr);
        }
    }
    return ubrList;
  }

  public String removeBoard(Integer id) {
    boardRepository.deleteById(id);

    return "Deleted board with id " + id.toString();
  }

  ///////////////

  public List<BoardColsResponse> getAllBoardCols() {
    List<User> u = userRepository.findAll();
    // List<Board> b = boardRepository.findAll();
    List<BoardColsResponse> bcrList = new ArrayList<>();
    Integer test = 0;

    for (User user : u) {
      List<Board> b = user.getBoards();
      for (Board board : b) {
        List<Col> c = board.getCols();
        for (Col col : c) {
          BoardColsResponse bcr = new BoardColsResponse();
          bcr.setId(col.getId().toString());
          bcr.setBoardName(board.getTitle());
          bcr.setStatus(col.getStatus());
          bcr.setTasks(board.getTasks().size()); // WIP; Takes all tasks in board but needs to take all tasks in cols
          bcrList.add(bcr);
        }
      }
    }
    return bcrList;
  }

  public String removeBoardCol(Integer id) {
    colRepository.deleteById(id);

    return "Deleted board col with id " + id.toString();
  }

  ///////////////

  public List<BoardTasksResponse> getAllBoardTasks() {
    List<User> u = userRepository.findAll();
    // List<Board> b = boardRepository.findAll();
    List<BoardTasksResponse> btrList = new ArrayList<>();

    for (User user : u) {
      List<Board> b = user.getBoards();
      for (Board board : b) {
          List<Task> t = board.getTasks();
          for (Task task: t){
            BoardTasksResponse btr = new BoardTasksResponse();
            btr.setId(task.getId().toString());
            btr.setBoardName(board.getTitle());
            btr.setTitle(task.getTitle());
            btr.setContent(task.getContent());
            btr.setStatus(task.getStatus());
            btrList.add(btr);
          }
        }
    }
    return btrList;
  }

  public String removeBoardTask(Integer id) {
    taskRepository.deleteById(id);

    return "Deleted board task with id " + id.toString();
  }
}