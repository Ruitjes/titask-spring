package titask.api.service;

import titask.api.entity.Task;
import titask.api.entity.dto.TaskDTO;
import titask.api.repository.TaskRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class TaskService {
    @Autowired
    private TaskRepository repository;
    
    public String deleteTask(int id) {
        repository.deleteById(id);
        return "Task removed !! " + id;
    }
    
    public String updateTask(int id, TaskDTO task) {
        Task persistentTask = new Task();

        persistentTask.setId(id);
        persistentTask.setIcon(task.getIcon());
        persistentTask.setStatus(task.getStatus());
        persistentTask.setTaskOrder(task.getTaskOrder());
        persistentTask.setTitle(task.getTitle());
        persistentTask.setContent(task.getContent());

        repository.save(persistentTask);

        return "Task updated" + id;

    }
}
