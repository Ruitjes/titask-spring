package titask.api.service;

import titask.api.entity.Board;
import titask.api.entity.Col;
import titask.api.entity.Task;
import titask.api.entity.User;
import titask.api.entity.dto.ColDTO;
import titask.api.entity.dto.TaskDTO;
import titask.api.exception.CustomException;
import titask.api.repository.BoardRepository;
import titask.api.repository.UserRepository;
import titask.api.security.JwtTokenProvider;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BoardService {
    @Autowired
    private BoardRepository repository;

    public BoardService(BoardRepository repository) {
        this.repository = repository;
    }

    String boardErrorMsg = "The board doesn't exist";
    
    public List<Board> getBoards() {
        return repository.findAll();
    }

    public Board getBoardById(int id) {
        Board board = repository.findById(id).orElse(null);
 
        if (board == null) {
            throw new CustomException(boardErrorMsg, HttpStatus.NOT_FOUND);
        }

        return board;
    }

    public String deleteBoard(int id) {
        repository.deleteById(id);

        return "board removed !! " + id;
    }

    public Board addColToBoard(int id, ColDTO col){
        Board board = repository.findById(id).orElse(null);
        
        Col persistentCol = new Col();
        persistentCol.setId(col.getId());
        persistentCol.setStatus(col.getStatus());
        persistentCol.setIcon(col.getIcon());
        persistentCol.setColor(col.getColor());

        if (board == null) {
            throw new CustomException(boardErrorMsg, HttpStatus.NOT_FOUND);
        }
          
        board.getCols().add(persistentCol);

        return repository.save(board);
    }

    public Board addTaskToBoard(int id, TaskDTO task){
        Board board = repository.findById(id).orElse(null);
        if (board == null) {
            throw new CustomException(boardErrorMsg, HttpStatus.NOT_FOUND);
        }

        Task persistentTask = new Task();
        persistentTask.setId(task.getId());
        persistentTask.setIcon(task.getIcon());
        persistentTask.setStatus(task.getStatus());
        persistentTask.setTaskOrder(task.getTaskOrder());
        persistentTask.setTitle(task.getTitle());
        persistentTask.setContent(task.getContent());

        board.getTasks().add(persistentTask);
        
        return repository.save(board);

    }
}
