package titask.api.service;

import java.nio.file.attribute.UserPrincipal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import titask.api.entity.Board;
import titask.api.entity.Role;
import titask.api.entity.User;
import titask.api.entity.adminResponse.UserResponse;
import titask.api.entity.dto.BoardDTO;
import titask.api.entity.dto.UserDTO;
import titask.api.exception.CustomException;
import titask.api.repository.BoardRepository;
import titask.api.repository.UserRepository;
import titask.api.security.JwtTokenProvider;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.config.web.server.ServerHttpSecurity.HttpsRedirectSpec;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import io.swagger.models.Response;

@Service
public class UserService {

  private UserRepository userRepository;
  private PasswordEncoder passwordEncoder;
  private JwtTokenProvider jwtTokenProvider;
  private AuthenticationManager authenticationManager;
  
  String userNotFoundMsg = "The user doesn't exist";

  public UserService(UserRepository userRepository, PasswordEncoder passwordEncoder, JwtTokenProvider jwtTokenProvider, AuthenticationManager authenticationManager) {
      this.userRepository = userRepository;
      this.passwordEncoder = passwordEncoder;
      this.jwtTokenProvider = jwtTokenProvider;
      this.authenticationManager = authenticationManager;
  }

  public User getUser(int id){
    User user = userRepository.findById(id).orElse(null);

    if (user == null) {
      throw new CustomException(userNotFoundMsg, HttpStatus.NOT_FOUND);
    }

    return user;
  }

  public User createNewBoard(Integer userId, BoardDTO board){
    User u = userRepository.findById(userId).orElse(null);

    if (u == null) {
      throw new CustomException(userNotFoundMsg, HttpStatus.NOT_FOUND);
    }
    List<Board> bList = u.getBoards();

    Board persistentBoard = new Board();
    persistentBoard.setId(board.getId());
    persistentBoard.setTitle(board.getTitle());
    persistentBoard.setDescription(board.getDescription());
    persistentBoard.setCols(board.getCols());
    persistentBoard.setTasks(board.getTasks());

    bList.add(persistentBoard);
    u.setBoards(bList);

    return userRepository.save(u);
  }

  public Boolean isItMyBoard(Integer userId, Integer boardId){   
    Boolean isItMyBoard = userRepository.existsByIdAndBoardsId(userId, boardId);
    return isItMyBoard;
  }

  public List<Board> getUserBoards(Integer userId) {
    User u = userRepository.findById(userId).orElse(null);
    
    if (u == null) {
      throw new CustomException(userNotFoundMsg, HttpStatus.NOT_FOUND);
    }

    return u.getBoards();
  }

  public String signin(String username, String password) {
    try {
      authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
      return jwtTokenProvider.createToken(username, userRepository.findByUsername(username).getRoles());
    } catch (AuthenticationException e) {
      throw new CustomException("Invalid username/password supplied", HttpStatus.UNPROCESSABLE_ENTITY);
    }
  }

  public String signup(UserDTO user) {
    if (!userRepository.existsByUsername(user.getUsername())) {
      user.setPassword(passwordEncoder.encode(user.getPassword()));
      
      User persistentUser = new User();
      persistentUser.setId(user.getId());   
      persistentUser.setUsername(user.getUsername());
      persistentUser.setEmail(user.getEmail());   
      persistentUser.setPassword(user.getPassword());
      persistentUser.setRoles(user.getRoles());   
      persistentUser.setBoards(user.getBoards());
      
      userRepository.save(persistentUser);

      return jwtTokenProvider.createToken(user.getUsername(), user.getRoles());
    } else {
      throw new CustomException("Username is already in use", HttpStatus.UNPROCESSABLE_ENTITY);
    }
  }

  public User whoami(HttpServletRequest req) {
    return userRepository.findByUsername(jwtTokenProvider.getUsername(jwtTokenProvider.resolveToken(req)));
  }

  public List<UserResponse> getAllUsers() {
    List<User> u = userRepository.findAll();
    List<UserResponse> urList = new ArrayList<>();

    for (User user : u) {
      UserResponse ur = new UserResponse();
      ur.setId(user.getId().toString());
      ur.setEmail(user.getEmail());
      ur.setUsername(user.getUsername());
      ur.setRole(user.getRoles().toString());
      ur.setBoards(user.getBoards().size());
      urList.add(ur);
    }

    return urList;
  }

  public String refresh(String username) {
    return jwtTokenProvider.createToken(username, userRepository.findByUsername(username).getRoles());
  }
}