package titask.api;

import java.util.ArrayList;
import java.util.Arrays;

import titask.api.entity.Role;
import titask.api.entity.User;
import titask.api.service.UserService;

import org.aspectj.apache.bcel.generic.InstructionConstants.Clinit;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class TiTaskAPI {
	
	@Autowired
	public
	UserService userService;
	public static void main(String[] args) {
		SpringApplication.run(TiTaskAPI.class, args);
	}

	@Bean
	public ModelMapper modelMapper() {
	  return new ModelMapper();
	}


}
