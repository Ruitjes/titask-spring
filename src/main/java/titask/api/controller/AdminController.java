package titask.api.controller;


import java.util.List;

import titask.api.entity.adminResponse.BoardColsResponse;
import titask.api.entity.adminResponse.BoardTasksResponse;
import titask.api.entity.adminResponse.UserBoardResponse;
import titask.api.entity.adminResponse.UserResponse;
import titask.api.service.AdminService;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;


@CrossOrigin(origins = {"http://localhost:4000", "https://titask-react-admin-panel.herokuapp.com"})
@RestController
public class AdminController {

  @Autowired
  private AdminService adminService;

  ///////////////

  @GetMapping(value = "/users")
  @PreAuthorize("hasRole('ROLE_ADMIN')")
  public List<UserResponse> getAllUsers() {
    return adminService.getAllUsers();
  }

  @DeleteMapping(value = "/users/delete/{id}")
  @PreAuthorize("hasRole('ROLE_ADMIN')")
  public String removeUser(@PathVariable(value = "id") Integer id) {
    return adminService.removeUser(id);
  }

  ///////////////

  @GetMapping(value = "/users/boards")
  @PreAuthorize("hasRole('ROLE_ADMIN')")
  public List<UserBoardResponse> getAllUsersBoards() {
    return adminService.getAllUsersBoards();
  }

  @DeleteMapping(value = "/boards/delete/{id}")
  @PreAuthorize("hasRole('ROLE_ADMIN')")
  public String removeBoard(@PathVariable(value = "id") int id) {
    return adminService.removeBoard(id);
  }

  ///////////////

  @GetMapping(value = "/boards/cols")
  @PreAuthorize("hasRole('ROLE_ADMIN')")
  public List<BoardColsResponse> getAllBoardCols() {
    return adminService.getAllBoardCols();
  }

  @DeleteMapping(value = "/boards/delete/col/{id}")
  @PreAuthorize("hasRole('ROLE_ADMIN')")
  public String removeBoardCol(@PathVariable(value = "id") int id) {
    return adminService.removeBoardCol(id);
  }

  ///////////////

  @GetMapping(value = "/boards/tasks")
  @PreAuthorize("hasRole('ROLE_ADMIN')")
  public List<BoardTasksResponse> getAllBoardTasks() {
    return adminService.getAllBoardTasks();
  }

  @DeleteMapping(value = "/boards/delete/task/{id}")
  @PreAuthorize("hasRole('ROLE_ADMIN')")
  public String removeBoardTask(@PathVariable(value = "id") int id) {
    return adminService.removeBoardTask(id);
  }
  /////////////


}