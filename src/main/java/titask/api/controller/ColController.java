package titask.api.controller;

import titask.api.entity.Col;
import titask.api.entity.dto.ColDTO;
import titask.api.service.ColService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin(origins = {"http://localhost:3000", "https://titask-react.herokuapp.com"})
@RestController
public class ColController {
    @Autowired
    ColService service;
 
    @DeleteMapping("/col/{colId}/delete")
    public String deleteCol(@PathVariable(value = "colId") Integer colId){
        service.deleteCol(colId);
        return "col with ID: " + colId + " is deleted";
    }

    @PutMapping("col/{colId}/update")
    public String updateCol( @PathVariable Integer colId, @RequestBody ColDTO col){
        return service.updateCol(colId, col);
    }
}
