package titask.api.controller;


import titask.api.entity.Task;
import titask.api.entity.dto.TaskDTO;
import titask.api.service.TaskService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin(origins = {"http://localhost:3000", "https://titask-react.herokuapp.com"})
@RestController
public class TaskController {
    @Autowired
    private TaskService service;

    @DeleteMapping("/task/{taskId}/delete")
    public String deleteTask(@PathVariable(value = "taskId") Integer taskId){
        service.deleteTask(taskId);
        return "Task with ID: " + taskId + " is deleted";
    }

    @PutMapping("task/{taskId}/update")
    public String updateTask( @PathVariable Integer taskId, @RequestBody TaskDTO task){
        return service.updateTask(taskId, task);
    }
}
