package titask.api.controller;

import java.util.List;

import javax.validation.Valid;

import titask.api.entity.Board;
import titask.api.entity.Col;
import titask.api.entity.Task;
import titask.api.entity.dto.ColDTO;
import titask.api.entity.dto.TaskDTO;
import titask.api.service.BoardService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin(origins = {"http://localhost:3000", "https://titask-react.herokuapp.com"})
@RestController
public class BoardController {

    @Autowired
    private BoardService service;

    @GetMapping("/boards")
    public List<Board> getAllBoards() {
        return service.getBoards();
    }

    @GetMapping("/board/{boardId}")
    public Board getBoard(@PathVariable(value = "boardId") Integer boardId) {
        return service.getBoardById(boardId);
    }

    @DeleteMapping("/board/{boardId}")
    public String deleteBoard(@PathVariable(value = "boardId") Integer boardId) {
        return service.deleteBoard(boardId);
    }

    @PostMapping("/board/{boardId}/col")
    public Board addCol(@PathVariable(value = "boardId") Integer boardId, @Valid @RequestBody ColDTO col){
        return service.addColToBoard(boardId, col);
    }

    @PostMapping("/board/{boardId}/task")
    public Board addTask(@PathVariable(value = "boardId") Integer boardId, @Valid @RequestBody TaskDTO task){        
        return service.addTaskToBoard(boardId, task);
    }

}
