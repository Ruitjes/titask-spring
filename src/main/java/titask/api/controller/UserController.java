package titask.api.controller;

import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import titask.api.entity.Board;
import titask.api.entity.User;
import titask.api.entity.dto.BoardDTO;
import titask.api.entity.dto.UserDTO;
import titask.api.service.UserService;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.ApiParam;

@CrossOrigin(origins = {"http://localhost:3000","http://localhost:4000", "https://titask-react.herokuapp.com", "https://titask-react-admin-panel.herokuapp.com"})
@RestController
public class UserController {

  @Autowired
  private UserService userService;

  @Autowired
  private ModelMapper modelMapper;

  @GetMapping("/user/{userId}/boards")
  public List<Board> getUserBoards(@PathVariable(value = "userId") Integer userId) {
      return userService.getUserBoards(userId);
  }

  @PostMapping("/user/{userId}/board")
  public User createNewBoard(@PathVariable(value = "userId") Integer userId, @Valid @RequestBody BoardDTO board) {
    return userService.createNewBoard(userId, board);
  }

  @GetMapping("/user/{userId}")
  public User getUser(@PathVariable(value = "userId") Integer userId) {
      return userService.getUser(userId);
  }
  
  @PostMapping("/user/signin")
  public String login(//
      @ApiParam("Username") @RequestParam String username, //
      @ApiParam("Password") @RequestParam String password) {
    return userService.signin(username, password);
  }

  @PostMapping("/user/signup")
  public String signup(@ApiParam("Signup User") @RequestBody UserDTO user) {
    return userService.signup(modelMapper.map(user, UserDTO.class));
  }

  @GetMapping("/user/refresh")
  @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_CLIENT')")
  public String refresh(HttpServletRequest req) {
    return userService.refresh(req.getRemoteUser());
  }

  @GetMapping(value = "/user/me")
  public User whoami(HttpServletRequest req) {
    return userService.whoami(req);
  }

  @GetMapping("/user/{userId}/myboard/{boardId}")
  public Boolean isItMyBoard(@PathVariable(value = "userId") Integer userId, @PathVariable(value = "boardId") Integer boardId) {
      Boolean isItMyBoard = userService.isItMyBoard(userId, boardId);
      return isItMyBoard;
  }
}