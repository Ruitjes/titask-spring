package titask.api.repository;

import titask.api.entity.Col;

import org.springframework.data.jpa.repository.JpaRepository;

public interface ColRepository extends JpaRepository<Col, Integer> {

}