package titask.api.repository;

import javax.transaction.Transactional;

import titask.api.entity.User;
import titask.api.entity.Board;


import org.springframework.data.jpa.repository.JpaRepository;

import antlr.collections.List;

public interface UserRepository extends JpaRepository<User, Integer> {
    Boolean existsByUsername(String username);

    User findByUsername(String username);
  
    @Transactional
    void deleteByUsername(String username);

    Boolean existsByIdAndBoardsId(Integer id, Integer boardId);
}
