package titask.api.repository;

import java.util.List;

import javax.transaction.Transactional;

import titask.api.entity.Task;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;


public interface TaskRepository extends JpaRepository<Task,Integer> {
    @Modifying
    @Transactional
    @Query("delete from Task t where t.status=:status")
    void deleteByStatus(@Param("status") String status);

    List<Task> findAllByOrderByTaskOrderAsc();
}

