package titask.api.entity;

import javax.persistence.*;
import javax.validation.constraints.Size;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "task")
public class Task {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    private Integer id;
    private String icon;
    private String status;
    private Integer taskOrder; // order is a reserverd word
    @Size(min = 3, max = 25)
    private String title;
    @Size(min = 3, max = 25)
    private String content;
}
