package titask.api.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.Singular;
import lombok.ToString;
import net.bytebuddy.build.Plugin.Engine.Source.Empty;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import org.springframework.lang.Nullable;

@Data
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "board")
public class Board {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    private Integer id;
    
    @Size(min = 3, max = 25)
    private String title;

    @Size(min = 3, max = 50)
    private String description;

    @OneToMany(targetEntity = Col.class,
    cascade = CascadeType.ALL, orphanRemoval=true)
    @JoinColumn(name ="cp_fk",referencedColumnName = "id")
    private List<Col> cols;
    
    @OneToMany(targetEntity = Task.class,
    cascade = CascadeType.ALL, orphanRemoval=true)
    @JoinColumn(name ="cp_fk",referencedColumnName = "id")
    private List<Task> tasks;
}
