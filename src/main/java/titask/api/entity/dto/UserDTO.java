package titask.api.entity.dto;

import java.util.List;

import titask.api.entity.Board;
import titask.api.entity.Role;

import lombok.Data;

@Data
public class UserDTO {
    private Integer id;
    private String username;
    private String email;
    private String password;
    private List<Role> roles;
    private List<Board> boards;
}
