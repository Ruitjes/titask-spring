package titask.api.entity.dto;

import lombok.Data;

@Data
public class ColDTO {
    private Integer id;
    private String status;
    private String icon;
    private String color;
}
