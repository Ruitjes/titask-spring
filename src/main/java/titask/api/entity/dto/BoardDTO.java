package titask.api.entity.dto;

import java.util.List;

import titask.api.entity.Col;
import titask.api.entity.Task;

import lombok.Data;

@Data
public class BoardDTO {
    private Integer id;
    private String title;
    private String description;
    private List<Col> cols;
    private List<Task> tasks;
}
