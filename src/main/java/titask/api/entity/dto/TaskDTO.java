package titask.api.entity.dto;

import lombok.Data;

@Data
public class TaskDTO {
    private Integer id;
    private String icon;
    private String status;
    private Integer taskOrder; // order is a reserverd word
    private String title;
    private String content;
}
