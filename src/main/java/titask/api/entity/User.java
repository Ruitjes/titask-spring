package titask.api.entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.validation.constraints.Size;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
  
    @Size(min = 4, max = 255, message = "Minimum username length: 4 characters")
    @Column(unique = true, nullable = false)
    private String username;
  
    @Column(unique = true, nullable = false)
    private String email;
  
    @Size(min = 5, message = "Minimum password length: 8 characters")
    private String password;
  
    @ElementCollection(fetch = FetchType.EAGER)
    List<Role> roles;
  
    @OneToMany(targetEntity = Board.class, cascade = CascadeType.ALL, orphanRemoval=true)
    @JoinColumn(name = "cp_fk" , referencedColumnName = "id")
    private List<Board> boards;
}
