package titask.api.entity.adminResponse;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class BoardColsResponse {
    private String id;
    private String boardName;
    private String status;
    private Integer tasks;
}

