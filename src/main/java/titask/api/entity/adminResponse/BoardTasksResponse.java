package titask.api.entity.adminResponse;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class BoardTasksResponse {
    private String id;
    private String boardName;
    private String title;
    private String content;
    private String status;

}
