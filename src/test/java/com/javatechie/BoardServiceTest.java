package com.javatechie;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import titask.api.entity.Board;
import titask.api.entity.Col;
import titask.api.entity.Role;
import titask.api.entity.Task;
import titask.api.entity.User;
import titask.api.entity.dto.BoardDTO;
import titask.api.entity.dto.ColDTO;
import titask.api.entity.dto.TaskDTO;
import titask.api.repository.BoardRepository;
import titask.api.repository.UserRepository;
import titask.api.security.JwtTokenProvider;
import titask.api.service.BoardService;
import titask.api.service.UserService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.crypto.password.PasswordEncoder;


@ExtendWith(MockitoExtension.class)
class BoardServiceTest {

	private BoardService service;

	@Mock
	private BoardRepository mockRepository;
	
	@BeforeEach
	public void setup() {
	  this.service = new BoardService(mockRepository);
	}

	List<Role> ROLES = new ArrayList<>();

	List<Col> COLS = new LinkedList<>(Arrays.asList(
		new Col(1, "Done", "x", "#EFEFEF")));

	List<Task> TASKS = new LinkedList<>(Arrays.asList(
		new Task(1, "x", "Doing", 1, "title", "description")));
		
	List<Board> BOARDS = new LinkedList<>(Arrays.asList(
		new Board(1, "bord1", "beschrijving1", COLS, TASKS),
		new Board(2, "bord2", "beschrijving2", COLS, TASKS)));

	List<User> USERS = new LinkedList<>(Arrays.asList(
		new User(1, "Stan", "stan@stan.nl", "stan123", ROLES, BOARDS),
		new User(2, "thomas", "thomas@thomas.nl", "thomas123", ROLES, BOARDS)));
	
	@Test
	public void getBoardsTest() {		
		when(mockRepository.findAll())
		.thenReturn(BOARDS);
		
		assertEquals(BOARDS, service.getBoards());
	}

	@Test
	public void getBoardByIdTest(){
		Integer boardId = 1;
		when(mockRepository.findById(boardId))
		.thenReturn(Optional.of(BOARDS.get(0)));
		
		assertEquals(BOARDS.get(0), service.getBoardById(boardId));
	}

	@Test
	public void addColToBoardTest(){
		Integer boardId = 1;
		Board board = new Board(1, "bord1", "beschrijving1", COLS, TASKS);
		
		Col col = new Col(1, "Done", "x", "#EFEFEF");
		ColDTO ColDTO = new ColDTO();
        ColDTO.setId(col.getId());
        ColDTO.setStatus(col.getStatus());
        ColDTO.setIcon(col.getIcon());
        ColDTO.setColor(col.getColor());

		//Find board
		when(mockRepository.findById(boardId))
			.thenReturn(Optional.of(board));

		//Save Board
		when(mockRepository.save(board))
			.thenReturn(board);
		
		assertEquals(2, service.addColToBoard(1, ColDTO).getCols().size());
	}

	@Test
	public void addTaskToBoardTest(){
		Integer boardId = 1;
		Board board = new Board(1, "bord1", "beschrijving1", COLS, TASKS);
		
		Task task = new Task(1, "x", "Doing", 1, "title", "description");
		TaskDTO TaskDTO = new TaskDTO();
        TaskDTO.setId(task.getId());
		TaskDTO.setIcon(task.getIcon());
		TaskDTO.setStatus(task.getStatus());
		TaskDTO.setTaskOrder(task.getTaskOrder());
		TaskDTO.setTitle(task.getTitle());
		TaskDTO.setContent(task.getContent());

		//Find board
		when(mockRepository.findById(boardId))
			.thenReturn(Optional.of(board));

		//Save Board
		when(mockRepository.save(board))
			.thenReturn(board);
		
		assertEquals(2, service.addTaskToBoard(1, TaskDTO).getTasks().size());
	}

	@Test
	public void deleteBoardTest(){
		Integer boardId = 1;

        // perform the call
        service.deleteBoard(boardId);

        // verify the mocks
		verify(mockRepository).deleteById(boardId); // check that the method was called
        // verify(mockRepository, times(1)).deleteById(eq(boardId));

	}
}
