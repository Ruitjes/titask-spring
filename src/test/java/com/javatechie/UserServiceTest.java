package com.javatechie;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import titask.api.entity.Board;
import titask.api.entity.Col;
import titask.api.entity.Role;
import titask.api.entity.Task;
import titask.api.entity.User;
import titask.api.entity.dto.BoardDTO;
import titask.api.repository.UserRepository;
import titask.api.security.JwtTokenProvider;
import titask.api.service.UserService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.crypto.password.PasswordEncoder;


@ExtendWith(MockitoExtension.class)
class UserServiceTest{

	private UserService userService;

	@Mock
	private UserRepository userMockRepository;

	@Mock
	private PasswordEncoder passwordEncoder;

	@Mock
	private JwtTokenProvider jwtTokenProvider;
	
	@Mock
	private AuthenticationManager authenticationManager;
	
	@BeforeEach
	public void setup() {
	  this.userService = new UserService(userMockRepository,passwordEncoder,jwtTokenProvider,authenticationManager);
	}
	List<Role> ROLES = new ArrayList<>();

	List<Col> COLS = new LinkedList<>(Arrays.asList(
		new Col(1, "Done", "x", "#EFEFEF")));

	List<Task> TASKS = new LinkedList<>(Arrays.asList(
		new Task(1, "x", "Doing", 1, "title", "description")));

	//Arrays.asList is fixed and cannot be chagned.
	List<Board> BOARDS = new LinkedList<>(Arrays.asList(
		new Board(1, "bord1", "beschrijving1", COLS, TASKS),
		new Board(2, "bord2", "beschrijving2", COLS, TASKS)));

	List<User> USERS = new LinkedList<>(Arrays.asList(
		new User(1, "Stan", "stan@stan.nl", "stan123", ROLES, BOARDS),
		new User(2, "thomas", "thomas@thomas.nl", "thomas123", ROLES, BOARDS)));
	
	@Test
	public void getUsersTest() {
		Integer userId = 1;
		User user = new User(1, "Stan", "stan@stan.nl", "stan123", ROLES, BOARDS);

		when(userMockRepository.findById(userId))
		.thenReturn(Optional.of(user));
		
		assertEquals(user, userService.getUser(userId));
	}

	@Test
	public void getAllUsersTest() {
		when(userMockRepository.findAll())
			.thenReturn(USERS);

		assertEquals(2, userService.getAllUsers().size());
	}

	@Test
	public void getUserBoardsTest() {
		int userId = 1;
		User user = new User(1, "Stan", "stan@stan.nl", "stan123", ROLES, BOARDS);

		when(userMockRepository.findById(userId))
		.thenReturn(Optional.of(user));
		
		assertEquals(2, userService.getUserBoards(userId).size());
	}

	@Test
	public void CreateNewBoardTest(){
		int userId = 1;
		User user = new User(1, "Stan", "stan@stan.nl", "stan123", ROLES, BOARDS);
		
		List<Col> COLS = new ArrayList<>();
		List<Task> TASKS = new ArrayList<>();

		Board board = new Board(1, "bord", "beschrijving", COLS, TASKS);
		BoardDTO boardDTO = new BoardDTO();
		boardDTO.setId(board.getId());
		boardDTO.setTitle(board.getTitle());
		boardDTO.setDescription(board.getDescription());
		boardDTO.setCols(board.getCols());
		boardDTO.setTasks(board.getTasks());

		//Find user
		when(userMockRepository.findById(userId))
			.thenReturn(Optional.of(user));
		
		// Add board to user
		// user.getBoards().add(board);
		
		// Save board to user
		when(userMockRepository.save(user))
			.thenReturn(user);
		
		assertEquals(3, userService.createNewBoard(userId, boardDTO).getBoards().size());
	}

	@Test
	public void isItMyBoard(){
		Integer userId = 1;
		Integer boardId = 1;
		
		when(userMockRepository.existsByIdAndBoardsId(userId, boardId))
		.thenReturn(true);

		assertEquals(true, userService.isItMyBoard(userId, boardId));
	}
}
